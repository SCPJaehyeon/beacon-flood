#pragma once
#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pcap/pcap.h>
#include <vector>
#define MAC_LEN 6
#define SSID_MAX_LEN 32
#define SR_LEN 6
#define NON 0x00
#define RT_LEN 0x0008
//#define RT_PS 0x00028000
#define RT_PS 0x00000000
#define BC_FC 0x0080
#define BC_TS 0x00004d2206d0185
#define BC_IT 0x0064
#define BC_CAP 0x0411
#define BC_SR_TYPE 0x01
#define RC_SR_LEN_I 4
#define BC_SR_LEN_H 0x04
#define BC_SR_1 0x82
#define BC_SR_2 0x84
#define BC_SR_5 0x8b
#define BC_SR_11 0x96

class Mac{
protected:
    u_char mac[MAC_LEN];
public:
    Mac() {};
    ~Mac() {};
    operator u_char*() const{
        return (u_char*)mac;
    }
};

struct ieee80211_radiotap_header {
    u_int8_t        it_version;     /* set to 0 */
    u_int8_t        it_pad;
    u_int16_t       it_len;         /* entire length */
    u_int32_t       it_present;     /* fields present */
} __attribute__((__packed__));

struct beacon_mac_header{
    u_int16_t       fc;
    u_int16_t       dr;
    Mac             dmac;
    Mac             smac;
    Mac             bssid;
    u_int16_t       seq;
};

struct beacon_header{
    u_int64_t      ts;
    u_int16_t      inter;
    u_int16_t      cap;
    u_int8_t       ssid_tn;
    u_int8_t       ssid_len;
    u_char         ssidnrates[SSID_MAX_LEN+SR_LEN];
};

#pragma pack(push, 1)
struct beacon_packet{
    ieee80211_radiotap_header radiotaph;
    beacon_mac_header beaconmach;
    beacon_header beaconh;
};
#pragma pack(pop)

void usage(char *argv);
int beacon_flood(char *dev, char *fname);
