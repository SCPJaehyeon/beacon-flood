TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -ltins
LIBS += -lpcap
SOURCES += \
    cpp/beacon-flood.cpp \
    cpp/main.cpp \
    cpp/print-info.cpp \
    cpp/test.cpp
HEADERS += \
    header/beacon-flood.h

