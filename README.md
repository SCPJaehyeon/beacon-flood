# beacon-flood

Joongbu University CCIT - Network



#### Usage

```shell
Usage:) ./beacon-flood <interface> <ssid-list-file>
Example:) ./beacon-flood wlan0 ssid_list.txt

$> vim ssid_list.txt
hello
why
so
serious
onemore
```

![1](/uploads/a22379157b8d40b9cc21250f2d2cb19c/1.PNG)