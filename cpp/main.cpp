#include "header/beacon-flood.h"

using namespace std;

int main(int argc, char *argv[])
{
    if(argc != 3){
        usage(argv[0]);
        return -1;
    }
    beacon_flood(argv[1], argv[2]);
    return 0;
}
