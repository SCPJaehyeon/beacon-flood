#include "header/beacon-flood.h"
using namespace std;

void usage(char *argv){
    cout << "Usage : " << argv << " [interface] [ssid-list-file]" << endl;
    cout << "Example) " << argv << " mon0 ssid-list.txt" << endl;
}
